<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Ui\Component\Edit\Form;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Kaliop\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
use Kaliop\Blog\Model\Post;

/**
 * Class StoreForm
 * @package Kaliop\Blog\Ui\Component\Edit
 */
class PostForm extends AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * StoreForm constructor.
     * @param PostCollectionFactory $postCollectionFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param array $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        PostCollectionFactory $postCollectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $postCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get form data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();
        /** @var Post $post */
        foreach ($items as $post) {
            $resultData = [
                'post' => [
                    'general' => $this->prepareStoreGeneral($post)
                ],
            ];
            $this->_loadedData[$post->getId()] = $resultData;
        }
        return $this->_loadedData;
    }

    /**
     * Get General store data
     *
     * @param Post $post
     * @return array
     */
    protected function prepareStoreGeneral($post)
    {
        $postData = $post->getData();
        $data = [
            'post_id' => $postData['post_id'],
            'is_active' => $postData['is_active'],
            'title' => $postData['title'],
            'content' => $postData['content'],
            'creation_time' => $postData['creation_time']
        ];

        return $data;
    }

}
