<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Ui\Component\Edit\Form;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Kaliop\Blog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

/**
 * Class CategoryForm
 * @package Kaliop\Blog\Ui\Component\Edit\Form
 */
class CategoryForm extends AbstractDataProvider
{
    /**
     * @var array
     */
    protected $_loadedData;

    /**
     * CategoryForm constructor.
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param string $name
     * @param string $primaryFieldName
     * @param array $requestFieldName
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        CategoryCollectionFactory $categoryCollectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $categoryCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $category) {
            $this->_loadedData[$category->getId()] = $category->getData();
        }
        return $this->_loadedData;
    }
}
