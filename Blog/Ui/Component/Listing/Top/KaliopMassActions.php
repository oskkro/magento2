<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Ui\Component\Listing\Top;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\MassAction;

/**
 * Class KaliopMassActions
 * @package Kaliop\Blog\Ui\Component\Listing\Column
 */
class KaliopMassActions extends MassAction
{
    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * KaliopMassActions constructor.
     * @param ContextInterface $context
     * @param AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        AuthorizationInterface $authorization,
        $components = [],
        array $data = []
    ) {
        $this->authorization = $authorization;
        $components = $this->restrictMassActions($components);
        if (empty($components)) {
            $data = [];
        }

        parent::__construct($context, $components, $data);
    }

    /**
     * Modify $components based on permissions if needed.
     *
     * @param array $components
     * @return array
     */
    protected function restrictMassActions($components)
    {
        foreach ($components as $key => $component) {
            if (!$this->_isActionAllowed($component['permissionResource']) && $component['permissionCanRemove']) {
                unset($components[$key]);
            }
        }

        return $components;
    }

    /**
     * Check if current user has edit action (resource provided in grid xml)
     *
     * @param string $resource
     * @return bool
     */
    protected function _isActionAllowed($resource)
    {
//        return $this->authorization->isAllowed($resource);
        return true;
    }
}
