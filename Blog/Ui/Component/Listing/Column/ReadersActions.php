<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 15:09
 */

namespace Kaliop\Blog\Ui\Component\Listing\Column;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns;

/**
 * Class CategoryActions
 * @package Kaliop\Blog\Ui\Component\Listing
 */
class ReadersActions extends Columns
{
    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * KaliopColumns constructor.
     * @param ContextInterface $context
     * @param AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        AuthorizationInterface $authorization,
        $components = [],
        array $data = []
    ) {
        $this->authorization = $authorization;
        $data = $this->restrictEditActions($data);
        parent::__construct($context, $components, $data);
    }

    /**
     * Modify $data based on permissions if needed.
     *
     * @param array $data
     * @return array
     */
    protected function restrictEditActions($data)
    {
        if (
            ($data['config']['editorConfig']['enabled'] ?? false)
            &&
            (!$this->_isActionAllowed($data['config']['editorConfig']['permissionResource']))
        ) {
            $data['config']['editorConfig']['enabled'] = false;
        }

        return $data;
    }

    /**
     * Check if current user has edit action (resource provided in grid xml)
     *
     * @param string $resource
     * @return bool
     */
    protected function _isActionAllowed($resource)
    {
        return true;
//        return $this->authorization->isAllowed($resource);
    }
}
