<?php
namespace Kaliop\Blog\Ui\Component\Listing\Column;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns;

/**
 * Class PostActions
 * @package Kaliop\Blog\Ui\Component\Listing
 */
class PostActions extends Columns
{
    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * KaliopColumns constructor.
     * @param ContextInterface $context
     * @param AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        AuthorizationInterface $authorization,
        $components = [],
        array $data = []
    ) {
        $this->authorization = $authorization;
        $data = $this->restrictEditActions($data);
        parent::__construct($context, $components, $data);
    }

    /**
     * Modify $data based on permissions if needed.
     *
     * @param array $data
     * @return array
     */
    protected function restrictEditActions($data)
    {
        if (
            ($data['config']['editorConfig']['enabled'] ?? false)
            &&
            (!$this->_isActionAllowed($data['config']['editorConfig']['permissionResource']))
        ) {
            $data['config']['editorConfig']['enabled'] = false;
        }

        return $data;
    }

    /**
     * Check if current user has edit action (resource provided in grid xml)
     *
     * @param string $resource
     * @return bool
     */
    protected function _isActionAllowed($resource)
    {
        return true;
//        return $this->authorization->isAllowed($resource);
    }
}
//
//use Magento\Framework\View\Element\UiComponent\ContextInterface;
//use Magento\Framework\View\Element\UiComponentFactory;
//use Magento\Ui\Component\Listing\Columns\Column;
//use Magento\Framework\UrlInterface;
//
//class PostActions extends Column
//{
//    /** Url path */
//    const BLOG_URL_PATH_EDIT = 'blog/post/edit';
//    const BLOG_URL_PATH_DELETE = 'blog/post/delete';
//
//    /** @var UrlInterface */
//    protected $urlBuilder;
//
//    /**
//     * @var string
//     */
//    private $editUrl;
//
//    /**
//     * @param ContextInterface $context
//     * @param UiComponentFactory $uiComponentFactory
//     * @param UrlInterface $urlBuilder
//     * @param array $components
//     * @param array $data
//     * @param string $editUrl
//     */
//    public function __construct(
//        ContextInterface $context,
//        UiComponentFactory $uiComponentFactory,
//        UrlInterface $urlBuilder,
//        array $components = [],
//        array $data = [],
//        $editUrl = self::BLOG_URL_PATH_EDIT
//    ) {
//        $this->urlBuilder = $urlBuilder;
//        $this->editUrl = $editUrl;
//        parent::__construct($context, $uiComponentFactory, $components, $data);
//    }
//
//    /**
//     * Prepare Data Source
//     *
//     * @param array $dataSource
//     * @return array
//     */
//    public function prepareDataSource(array $dataSource)
//    {
//        if (isset($dataSource['data']['items'])) {
//            foreach ($dataSource['data']['items'] as & $item) {
//                $name = $this->getData('name');
//                if (isset($item['post_id'])) {
//                    $item[$name]['edit'] = [
//                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['post_id' => $item['post_id']]),
//                        'label' => __('Edit')
//                    ];
//                    $item[$name]['delete'] = [
//                        'href' => $this->urlBuilder->getUrl(self::BLOG_URL_PATH_DELETE, ['post_id' => $item['post_id']]),
//                        'label' => __('Delete'),
//                        'confirm' => [
//                            'title' => __('Delete "${ $.$data.title }"'),
//                            'message' => __('Are you sure you wan\'t to delete a "${ $.$data.title }" record?')
//                        ]
//                    ];
//                }
//            }
//        }
//
//        return $dataSource;
//    }
//}
