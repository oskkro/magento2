<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Kaliop
 * @package Kaliop_Blog
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Ui\Component\Listing\Column;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class Actions
 * @package Kaliop\Blog\Ui\Component\Listing\Column
 */
class Actions extends Column
{
    /**
     * ACL resource name
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::blog_{%modelName%}_save';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var AuthorizationInterface
     */
    protected $auth;

    /**
     * @var string
     */
    protected $modelName;

    /**
     * Actions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        AuthorizationInterface $authorization,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->auth = $authorization;
        $this->modelName = $data['config']['modelName'];
        $data = $this->_isAllowed() ? $data : [];
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if ($this->_isAllowed() && isset($dataSource['data']['items'])) {
            $id = $this->context->getFilterParam($this->getConfiguration()['indexField']);

            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        "kaliop_blog/{$this->getConfiguration()['modelName']}/edit",
                        ['id' => $item[$item['id_field_name']], $this->getConfiguration()['modelName'] => $id]
                    ),
                    'label' => __('Edit'),
                    'hidden' => false,
                ];
            }
        }

        return $dataSource;
    }

    /**
     * Check if actions are allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
//        $permissionName = str_replace('{%modelName%}', $this->modelName,
//            self::PERMISSION_RESOURCE_NAME);
//
//        return $this->auth->isAllowed($permissionName);
        return true;
    }
}
