<?php

namespace Kaliop\Blog\Controller;

use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Kaliop\Blog\Helper\Data;

class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;
    /**
     * Blog helper
     *
     * @var Data
     */
    protected $blogHelper;

    /**
     * @param ActionFactory $actionFactory
     * @param Data $blogHelper
     */
    public function __construct(
        ActionFactory $actionFactory,
        Data $blogHelper
    ) {
        $this->actionFactory = $actionFactory;
        $this->blogHelper = $blogHelper;

    }

    /**
     * Match application action by request
     *
     * @param RequestInterface $request
     * @return ActionInterface
     */
    public function match(RequestInterface $request)
    {
        if ($this->_match($request)) {
            return $this->actionFactory->create(
                'Magento\Framework\App\Action\Forward',
                ['request' => $request]
            );
        } else {
            return null;
        }
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param RequestInterface $request
     * @return bool
     */
    public function _match(RequestInterface $request)
    {
        /**
         * When forwarding to 404 we cannot let Router to match
         */
        if ($request->getModuleName() === 'kaliop_blog') {
            return false;
        }
        $identifier = trim($request->getPathInfo(), '/');
        $urlKey = $this->blogHelper->getUrlKey();

        /**
         * View page
         */
        if (preg_match("#{$urlKey}(/([^/]+))?.*$#isu", $identifier, $matches) && count($matches) > 2) {
            $request->setModuleName('kaliop_blog')
                ->setControllerName('post')
                ->setActionName('view')
                ->setParam('id', $matches[2]);
//            var_dump('test');die;
            return true;
        }
//        /**
//         * Listing page
//         */
//        if (preg_match("#{$urlKey}(/$|$)#isu", $identifier)) {
//            $request->setModuleName('kaliop_blog')
//                ->setControllerName('post')
//                ->setActionName('index');
//
//            return true;
//        }
        return false;
    }
}
