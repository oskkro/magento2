<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Post;

use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\PostRepository;
use Kaliop\Blog\Model\PostRepositoryFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Kaliop\Blog\Block\Post\ViewPost;
//use Kaliop\Blog\Controller\Post\ChangeStatus;

class View extends Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var StatusManager
     */
    public $statusManager;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ForwardFactory $resultForwardFactory,
        Registry $registry,
        PostFactory $postFactory,
        StatusManager $statusManager
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->registry = $registry;
        $this->postFactory = $postFactory;
        $this->statusManager = $statusManager;
    }

    public function execute()
    {
        /** @var Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        $postModel = $this->statusManager->_initItem();

            if (!$postModel) {
                $this->getRequest()->setDispatched(false);
                return $resultForward->forward('noroute');
            } else {
                $this->statusManager->processRequest($postModel);
            }

        /** @var Page $resultPage */
        $resultPage = $this->pageFactory->create();

        return $resultPage;
    }

}
