<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 15:33
 */

namespace Kaliop\Blog\Controller\Post;

use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Read extends Action
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var StatusManager
     */
    public $statusManager;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Registry $registry,
        PostFactory $postFactory,
        StatusManager $statusManager
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->registry = $registry;
        $this->postFactory = $postFactory;
        $this->statusManager = $statusManager;
    }

    public function execute()
    {
        $postModel = $this->statusManager->_initItem();

        if ($postModel != false) {
            $this->statusManager->processRequest($postModel);
        }

        $resultPage = $this->pageFactory->create();
        return $resultPage;
    }

}
