<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 23.06.17
 * Time: 14:33
 */

namespace Kaliop\Blog\Controller\Post;

use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Magento\Backend\App\Action\Context;
use Magento\Cms\Model\PageFactory;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;


/**
 * @return ResponseInterface|Redirect
 */
class StatusManager
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var postFactory
     */
    private $postFactory;

    /**
     * @var SessionFactory
     */
    private $customerSession;

    /**
     * @var RequestInterface
     */
    private $_request;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * SubscribeStore constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param PostFactory $postFactory
     * @param RequestInterface $request
     * @param Registry $registry
     * @param ResultFactory $resultFactory
     * @param SessionFactory $customerSession
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        PostFactory $postFactory,
        RequestInterface $request,
        ResultFactory $resultFactory,
        Registry $registry,
        SessionFactory $customerSession
    ) {
        $this->registry = $registry;
        $this->pageFactory = $pageFactory;
        $this->postFactory = $postFactory;
        $this->_request = $request;
        $this->customerSession = $customerSession;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
    }

    public function processRequest(Post $postModel)
    {
        $post = $this->_request->getParams();
        $customerSession = $this->customerSession->create();

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setRefererUrl();


        if ($customerSession->isLoggedIn()) {
            $data = [
                'post_id' => $postModel->getId(),
                'entity_id' => $customerSession->getCustomerId()
            ];
            if (isSet($post['setRead'])) {
                $postModel->setSeen($data);
                return $resultRedirect;
            } else if (isSet($post['unsetRead'])) {
                $postModel->unsetSeen($data);
                return $resultRedirect;
            } else {
                return true;
            }
        }

    }

    public function _initItem()
    {
        $itemId = $this->_request->getParam('id');

        if (!$itemId) {
            return false;
        }

        /**
         * @var Post $postModel
         */
        $postModel = $this->postFactory->create();

        /**
         * Load by url_key
         */
        $postModel->getResource()->load($postModel, $itemId, 'url_key');
        if (!$postModel->getId()) {
            /**
             * Load by id
             */
            $postModel->getResource()->load($postModel, $itemId);
        }

        if (!$postModel->getId()) {
            return false;
        }

        $this->registry->unregister(Post::ENTITY_REGISTRY_NAME);
        $this->registry->register(Post::ENTITY_REGISTRY_NAME, $postModel);

        return $postModel;
    }

}
