<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

/**
 * Class Categorygrid
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class Categorygrid extends Categories
{
    /**
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        return parent::execute();
    }
}
