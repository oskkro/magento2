<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as PostAction;
use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\LayoutFactory;
use Zend\Http\Request;

/**
 * Class Categories
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class Categories extends PostAction
{
    /**
     * @var LayoutFactory $resultLayoutFactory
     */
    private $resultLayoutFactory;

    /**
     * @var Registry $coreRegistry
     */
    private $coreRegistry;

    /**
     * @var PostFactory $postFactory
     */
    private $postFactory;

    /**
     * Categories constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param PostFactory $postFactory
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        LayoutFactory $resultLayoutFactory,
        PostFactory $postFactory,
        Registry $registry
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->postFactory = $postFactory;
        $this->coreRegistry = $registry;
    }

    /**
     * Store categories grid
     *
     * @return \Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        /** @var RequestInterface $requestInterface */
        $requestInterface = $this->getRequestInterface();

        if ($requestInterface->getParam('id')) {
            $postModel = $this->postFactory->create();
            $postModel->getResource()->load($postModel, $requestInterface->getParam('id'));
            $this->coreRegistry->unregister(Post::ENTITY_REGISTRY_NAME);
            if ($postModel) {
                $this->coreRegistry->register(Post::ENTITY_REGISTRY_NAME, $postModel);
            }
        }

        /** @var Request $request */
        $request = $this->getRequest();

        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('kaliop_blog.post.tab.edit.post_categorygrid')
            ->setData('categories', $request->getPost('categories', null));
        return $resultLayout;
    }
}
