<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Magento\Framework\Registry;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as BlogAction;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\App\Action\Context;
use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class Edit
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class Edit extends BlogAction
{
    /**
     * @var Registry $coreRegistry
     */
    private $coreRegistry;

    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * Edit constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        Registry $registry
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->postFactory = $postFactory;
        $this->coreRegistry = $registry;
    }

    /**
     * @return Page
     */
    public function execute()
    {
        $reqParams = $this->getRequestInterface()->getParams();

        $postModel = $this->postFactory->create();

        if (isset($reqParams['id'])) {
            $postModel->getResource()->load($postModel, $reqParams['id']);
        }
        $this->coreRegistry->unregister(Post::ENTITY_REGISTRY_NAME);
        $this->coreRegistry->register(Post::ENTITY_REGISTRY_NAME, $postModel);

        /** @var Page $resultPage */
        $resultPage = $this->getResultPageFactory()->create();

        $resultPage->setActiveMenu('Kaliop_Blog::post');

        $resultPage->addBreadcrumb(__('Blog Posts'), __('Blog Posts'));
        $resultPage->addBreadcrumb(__('List'), __('List'));

        if ($postModel->getId()) {
            $resultPage->addBreadcrumb(__('Edit Blog Post'), __('Edit Blog Post'));
            $resultPage->getConfig()->getTitle()->prepend('Edit Blog Post');
        } else {
            $resultPage->addBreadcrumb(__('New Blog Post'), __('New Blog Post'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Blog Post'));
        }

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
