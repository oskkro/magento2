<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Kaliop\Blog\Ui\Component\Listing\Column\PostActions;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as PostAction;
use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\PostRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class MassAction
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class MassAction extends PostAction
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * MassAction constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PostRepository $postRepository
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PostRepository $postRepository,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->postFactory = $postFactory;
        $this->redirectFactory = $redirectFactory;
        $this->postRepository = $postRepository;
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $result = null;
        /** @var Http $request */
        $request = $this->getRequestInterface();
        $reqParams = $request->getParams();

        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('kaliop_blog/post/index');

        $messageManager = $this->getMessageManager();
        $allowedMassStatusses = [Post::STATUS_DISABLED, Post::STATUS_ENABLED];
        if (!isset($reqParams['status']) || (!isset($reqParams['selected']) && !isset($reqParams['excluded']))) {
            $messageManager->addErrorMessage(__('Please make sure, your parameter data is correct.'));
            return $resultRedirect;
        }

        $status = $reqParams['status'];
        if (!in_array($status, $allowedMassStatusses)) {
            $messageManager->addErrorMessage(__('Incorrect status choosed.'));
            return $resultRedirect;
        }

        $postIds = $reqParams['selected'] ?? $reqParams['excluded'];
        $actionIncluded = isset($reqParams['selected']) ? true : false;

        $this->postRepository->changePostStatus($status, $postIds, $actionIncluded);
        $messageManager->addSuccessMessage(__('Post status changed successfully!'));

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
