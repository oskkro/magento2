<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Kaliop\Blog\Ui\Component\Listing\Column\PostActions;
use Magento\Framework\Controller\Result\Redirect;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as PostAction;

use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\PostRepository;
use Kaliop\Blog\Model\PostRepositoryFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class Delete
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class Delete extends PostAction
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var PostRepositoryFactory
     */
    private $postRepository;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;


    /**
     * @var Filter
     */
    protected $filter;

    /**
     * Delete constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PostRepositoryFactory $postRepository
     * @param Filter $filter
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PostRepositoryFactory $postRepository,
        Filter $filter,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);

        $this->postFactory = $postFactory;
        $this->redirectFactory = $redirectFactory;
        $this->postRepository = $postRepository;
        $this->filter = $filter;
    }

    /**
     * Dispatch request
     *
     * @return ResultInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        /** @var PostRepository $repository */
        $repository = $this->postRepository->create();

        /** @var RequestInterface */
        $request = $this->getRequest();
        $requestParams = $request->getParams();

        $result = false;
        $isMassDelete = $this->isMassDelete($request);
        if (!$isMassDelete && isset($requestParams['id'])) {
            $result = $repository->deleteById($requestParams['id']);
        } elseif ($isMassDelete) {
            /** @var Post $post */
            $post = $this->postFactory->create();
            $filteredCollection = $this->filter->getCollection($post->getCollection());
//            var_dump($filteredCollection->count());die;
            if ($filteredCollection->count() > 0) {
                foreach ($filteredCollection as $item) {
                    $repository->delete($item);
                }
                $result = true;
            }
        }

        return $this->resultRedirect($result, $isMassDelete);
    }

    /**
     * @param RequestInterface $request
     * @return bool
     */
    protected function isMassDelete(RequestInterface $request)
    {
        $params = $request->getParams();
        return isset($params['filters']);
    }

    /**
     * Redirect with message.
     *
     * @param bool $result
     * @param bool $isMassDelete
     *
     * @return Redirect
     */
    protected function resultRedirect($result, $isMassDelete = false)
    {
        /** @var ManagerInterface $messageManager */
        $messageManager = $this->getMessageManager();
        if ($result) {
            if ($isMassDelete) {
                $messageManager->addSuccessMessage(__('Removed selected posts'));
            } else {
                $messageManager->addSuccessMessage(__('Post removed'));
            }
        } else {
            $messageManager->addErrorMessage(__('There was an error processing remove action. Please try again.'));
        }
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('kaliop_blog/post/index');

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_delete');
    }
}
