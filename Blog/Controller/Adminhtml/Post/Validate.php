<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\DataObject;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\Post;
use Magento\Backend\App\Action\Context;

/**
 * Class Validate
 * @package Kaliop\Blog\Controller\Adminhtml\Post
 */
class Validate extends Action
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var Product
     */
    private $productModel;

    /**
     * Save constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param JsonFactory $resultJsonFactory
     * @param ProductFactory $productFactory
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        JsonFactory $resultJsonFactory,
        ProductFactory $productFactory
    ) {
        parent::__construct($context);
        $this->postFactory = $postFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productFactory = $productFactory;
        
        $this->productModel = $this->productFactory->create();
    }

    /**
     * AJAX validation action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = new DataObject();
        $response->setError(0);

//        $this->_validateUrlKey($response);
        $resultJson = $this->resultJsonFactory->create();
        if ($response->getError()) {
            $response->setError(true);
            $response->setMessages($response->getMessages());
        }

        $resultJson->setData($response);
        return $resultJson;
    }
//
//    /**
//     * Store validation
//     *
//     * @param DataObject $response
//     */
//    protected function _validateUrlKey($response)
//    {
//        $requestParams = $this->getRequest()->getParams();
//        $urlKey = isset($requestParams['store']['general']['url_key']) ? $requestParams['post']['general']['url_key'] : null;
//
//        if (is_null($urlKey) || empty($urlKey)) {
//            $response->setMessages([__('Url key not provided')]);
//            $response->setError(1);
//            return;
//        }
//
//        $urlKey = $this->productModel->formatUrlKey($urlKey);
//
//        /** @var Store $store */
//        $store = $this->storeFactory->create();
//        $storeId = isset($requestParams['store']['general']['storelocator_store_id']) ?
//            $requestParams['store']['general']['storelocator_store_id'] : false;
//
//        /**
//         * Check if url key has not been assigned to another entity
//         */
//        $readConnection = $store->getResource()->getConnection();
//        $select = $readConnection
//            ->select()
//            ->from('soon_storelocator_store', array('storelocator_store_id'))
//            ->where($readConnection->quoteInto('url_key = ?', $urlKey));
//
//        if ($storeId) {
//            $select->where($readConnection->quoteInto("storelocator_store_id != ?", $storeId));
//        }
//
//        if ($readConnection->fetchOne($select)) {
//            $response->setMessages([__('Duplicated Url Key')]);
//            $response->setError(1);
//        }
//    }
}
