<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Post;

use Kaliop\Blog\Model\Post;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as StoreAction;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\PostRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class Save
 */
class Save extends StoreAction
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param PostFactory $postFactory
     * @param PostRepository $postRepository
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        PostFactory $postFactory,
        PostRepository $postRepository,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->postFactory = $postFactory;
        $this->redirectFactory = $redirectFactory;
        $this->postRepository = $postRepository;
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $result = null;
        /** @var Http $request */
        $request = $this->getRequestInterface();
        $reqParams = $request->getParams();
        $messageManager = $this->getMessageManager();

        $result = $this->postRepository->processAndSaveFromRequest($reqParams);

        if ($result['error'] || is_null($result)) {
            $messageManager->addErrorMessage(__('There was an error processing request. Please Try again.'));
        } else {
            $messageManager->addSuccessMessage($result['message']);
        }

        $resultRedirect = $this->redirectFactory->create();
        if ($this->_request->getParam('back') == 'edit' && $result['id']) {
            $resultRedirect->setPath('kaliop_blog/post/edit/id/' . $result['id']);
        } else {
            $resultRedirect->setPath('kaliop_blog/post');
        }


        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
