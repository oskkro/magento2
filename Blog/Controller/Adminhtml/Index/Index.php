<?php

namespace Kaliop\Blog\Controller\Adminhtml\Index;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        //Call page factory to render layout and page content
        $this->_setPageData();
        return $this->getResultPage();
    }

    /*
     * Check permission via ACL resource
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Kaliop_Blog::post');
    }

    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Kaliop_Blog::post');
        $resultPage->getConfig()->getTitle()->prepend((__('Blog')));

        //Add bread crumb
        $resultPage->addBreadcrumb(__('Kaliop'), __('Kaliop'));
        $resultPage->addBreadcrumb(__('Kaliop'), __('Blog'));

        return $this;
    }


}
///**
// * @package Kaliop_Blog
// */
//
//namespace Kaliop\Blog\Controller\Index;
//
////use Magento\Framework\App\Action\Action;
//use Magento\Backend\App\Action;
////use Magento\Framework\App\Action\Context;
//use Magento\Backend\App\Action\Context;
////use Magento\Framework\View\Result\PageFactory;
//use Magento\Framework\View\Result\PageFactory;
//
//class Index extends Action
//{
//    /**
//     * @var PageFactory
//     */
//    protected $_resultPageFactory;
//
//    public function __construct(
//        Context $context,
//        PageFactory $resultPageFactory
//    )
//    {
//        parent::__construct($context);
//        $this->_resultPageFactory = $resultPageFactory;
//    }
//
//    public function execute()
//    {
//        $resultPage = $this->_resultPageFactory->create();
//        $resultPage->setActiveMenu('Kaliop_Blog::post');
//        $resultPage->getConfig()->getTitle()->prepend(__('Blog'));
//        return $resultPage;
//    }
//
//}
