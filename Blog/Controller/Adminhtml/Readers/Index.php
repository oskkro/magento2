<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 14:51
 */

namespace Kaliop\Blog\Controller\Adminhtml\Readers;


use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {

        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Kaliop_Blog::readers');
        $resultPage->addBreadcrumb(__('Blog Readers'), __('Blog Readers'));
        $resultPage->addBreadcrumb(__('Manage Blog Readers'), __('Manage Blog Readers'));
        $resultPage->getConfig()->getTitle()->prepend(__('Blog Readers'));

        return $resultPage;
    }
}
