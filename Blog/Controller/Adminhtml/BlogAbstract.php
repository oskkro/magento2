<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class StoreLocatorAbstract
 * @package Kaliop\Blog\Controller\Adminhtml
 */
abstract class BlogAbstract extends Action
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var ConfigFactory
     */
    private $pageConfigFactory;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * @var RequestInterface
     */
    private $requestInterface;

    /**
     * @var ManagerInterface
     */
    private $messageManagerInterface;

    /**
     * StoreLocatorAbstract constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->pageConfigFactory = $pageConfigFactory;
        $this->resultForwardFactory = $forwardFactory;
        $this->requestInterface = $context->getRequest();
        $this->messageManagerInterface = $context->getMessageManager();

        parent::__construct($context);
    }

    /**
     * @return RequestInterface
     */
    public function getRequestInterface()
    {
        return $this->requestInterface;
    }

    /**
     * @return ConfigFactory
     */
    public function getPageConfigFactory()
    {
        return $this->pageConfigFactory;
    }

    /**
     * @return PageFactory
     */
    public function getResultPageFactory()
    {
        return $this->resultPageFactory;
    }

    /**
     * @return ForwardFactory
     */
    public function getResultForwardFactory()
    {
        return $this->resultForwardFactory;
    }

    /**
     * @return ManagerInterface
     */
    public function getMessageManagerInterface()
    {
        return $this->messageManagerInterface;
    }
}
