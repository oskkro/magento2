<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\DataObject;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\Category;
use Magento\Backend\App\Action\Context;

/**
 * Class Validate
 * @package Kaliop\Blog\Controller\Adminhtml\Category
 */
class Validate extends Action
{
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var Product
     */
    private $productModel;

    /**
     * Save constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param JsonFactory $resultJsonFactory
     * @param ProductFactory $productFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        JsonFactory $resultJsonFactory,
        ProductFactory $productFactory
    )
    {
        parent::__construct($context);
        $this->categoryFactory = $categoryFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productFactory = $productFactory;

        $this->productModel = $this->productFactory->create();
    }

    /**
     * AJAX validation action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = new DataObject();
        $response->setError(0);

//        $this->_validateUrlKey($response);
        $resultJson = $this->resultJsonFactory->create();
        if ($response->getError()) {
            $response->setError(true);
            $response->setMessages($response->getMessages());
        }

        $resultJson->setData($response);
        return $resultJson;
    }
}
