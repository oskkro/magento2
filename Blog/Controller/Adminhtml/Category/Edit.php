<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Category;

use Magento\Framework\Registry;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as BlogAction;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\App\Action\Context;
use Kaliop\Blog\Model\Category;
use Kaliop\Blog\Model\CategoryFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class Edit
 * @package Kaliop\Blog\Controller\Adminhtml\Category
 */
class Edit extends BlogAction
{
    /**
     * @var Registry $coreRegistry
     */
    private $coreRegistry;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * Edit constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        Registry $registry
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->categoryFactory = $categoryFactory;
        $this->coreRegistry = $registry;
    }

    /**
     * @return Page
     */
    public function execute()
    {
        $reqParams = $this->getRequestInterface()->getParams();

        $categoryModel = $this->categoryFactory->create();

        if (isset($reqParams['id'])) {
            $categoryModel->getResource()->load($categoryModel, $reqParams['id']);
        }
        $this->coreRegistry->unregister(Category::ENTITY_REGISTRY_NAME);
        $this->coreRegistry->register(Category::ENTITY_REGISTRY_NAME, $categoryModel);

        /** @var Page $resultPage */
        $resultPage = $this->getResultPageFactory()->create();

        $resultPage->setActiveMenu('Kaliop_Blog::category');

        $resultPage->addBreadcrumb(__('Blog Categories'), __('Blog Categories'));
        $resultPage->addBreadcrumb(__('List'), __('List'));

        if ($categoryModel->getId()) {
            $resultPage->addBreadcrumb(__('Edit Blog Category'), __('Edit Blog Category'));
            $resultPage->getConfig()->getTitle()->prepend('Edit Blog Category');
        } else {
            $resultPage->addBreadcrumb(__('New Blog Category'), __('New Blog Category'));
            $resultPage->getConfig()->getTitle()->prepend(__('New Blog Category'));
        }

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
