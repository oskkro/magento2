<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Category;

use Magento\Backend\Model\View\Result\Forward;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as BlogAction;

/**
 * Class NewCategory
 * @package Kaliop\Blog\Controller\Adminhtml\Category
 */
class NewCategory extends BlogAction
{
    /**
     * @return Forward
     */
    public function execute()
    {
        $resultForward = $this->getResultForwardFactory()->create();
        $resultForward->forward('edit');
        return $resultForward;
    }
}
