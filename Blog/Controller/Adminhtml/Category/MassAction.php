<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Category;

use Kaliop\Blog\Ui\Component\Listing\Column\CategoryActions;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as CategoryAction;
use Kaliop\Blog\Model\Category;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\CategoryRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class MassAction
 * @package Kaliop\Blog\Controller\Adminhtml\Category
 */
class MassAction extends CategoryAction
{
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * MassAction constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepository $categoryRepository
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->categoryFactory = $categoryFactory;
        $this->redirectFactory = $redirectFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $result = null;
        /** @var Http $request */
        $request = $this->getRequestInterface();
        $reqParams = $request->getParams();

        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('kaliop_blog/category/index');

        $messageManager = $this->getMessageManager();
        $allowedMassStatusses = [Category::STATUS_DISABLED, Category::STATUS_ENABLED];
        if (!isset($reqParams['status']) || (!isset($reqParams['selected']) && !isset($reqParams['excluded']))) {
            $messageManager->addErrorMessage(__('Please make sure, your parameter data is correct.'));
            return $resultRedirect;
        }

        $status = $reqParams['status'];
        if (!in_array($status, $allowedMassStatusses)) {
            $messageManager->addErrorMessage(__('Incorrect status choosed.'));
            return $resultRedirect;
        }

        $categoryIds = $reqParams['selected'] ?? $reqParams['excluded'];
        $actionIncluded = isset($reqParams['selected']) ? true : false;

        $this->categoryRepository->changeCategoryStatus($status, $categoryIds, $actionIncluded);
        $messageManager->addSuccessMessage(__('Category status changed successfully!'));

        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
