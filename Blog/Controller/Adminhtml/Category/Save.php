<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Controller\Adminhtml\Category;

use Kaliop\Blog\Model\ResourceModel\Category;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Kaliop\Blog\Controller\Adminhtml\BlogAbstract as StoreAction;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\CategoryRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\Page\ConfigFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

/**
 * Class Save
 */
class Save extends StoreAction
{
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepository $categoryRepository
     * @param PageFactory $resultPageFactory
     * @param ConfigFactory $pageConfigFactory
     * @param ForwardFactory $forwardFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        PageFactory $resultPageFactory,
        ConfigFactory $pageConfigFactory,
        ForwardFactory $forwardFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context, $resultPageFactory, $pageConfigFactory, $forwardFactory);
        $this->categoryFactory = $categoryFactory;
        $this->redirectFactory = $redirectFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        $result = null;
        /** @var Http $request */
        $request = $this->getRequestInterface();
        $reqParams = $request->getParams();
        $messageManager = $this->getMessageManager();
//        var_dump($reqParams);die;

        $result = $this->categoryRepository->processAndSaveFromRequest($reqParams);
//        $result = ['error'=>'tak'];
        if ($result['error'] || is_null($result)) {
            $messageManager->addErrorMessage(__('There was an error processing request. Please Try again.'));
        } else {
            $messageManager->addSuccessMessage($result['message']);
        }

        $resultRedirect = $this->redirectFactory->create();
        if ($this->_request->getParam('back') == 'edit' && $result['id']) {
            $resultRedirect->setPath('kaliop_blog/category/edit/id/' . $result['id']);
        } else {
            $resultRedirect->setPath('kaliop_blog/category');
        }


        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
//        return $this->_authorization->isAllowed('Kaliop_Blog::blog_post_save');
    }
}
