<?php
/**
 * Hello Rewrite Product View Controller
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Kaliop\Blog\Controller\Rewrite\Product;

use Magento\Catalog\Controller\Product;

class View extends Product\View
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // Do your stuff here
        return parent::execute();
    }
}
