<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 14:22
 */

namespace Kaliop\Blog\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Readers extends AbstractModel implements IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'post_readers';

    protected function _construct()
    {
        $this->_init('Kaliop\Blog\Model\ResourceModel\Readers');
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
