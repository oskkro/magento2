<?php

namespace Kaliop\Blog\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Blog post mysql resource
 */
class Post extends AbstractDb
{

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param string|null $resourcePrefix
     */
    public function __construct(
        Context $context,
        DateTime $date,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('kaliop_blog_post', 'post_id');
    }

    /**
     * Process post data before saving
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {

        if (!$this->isValidPostUrlKey($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The post URL key contains capital letters or disallowed symbols.')
            );
        }

        if ($this->isNumericPostUrlKey($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The post URL key cannot be made of only numbers.')
            );
        }

        if ($object->isObjectNew() && !$object->hasCreationTime()) {
            $object->setCreationTime($this->_date->gmtDate());
        }

        $object->setUpdateTime($this->_date->gmtDate());

        return parent::_beforeSave($object);
    }

    /**
     * Load an object using 'url_key' field if there's no field specified and value is not numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed $value
     * @param string $field
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'url_key';
        }

        return parent::load($object, $value, $field);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param \Kaliop\Blog\Model\Post $object
     * @return \Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        if ($object->getStoreId()) {

            $select->where(
                'is_active = ?',
                1
            )->limit(
                1
            );
        }

        return $select;
    }

    /**
     * Retrieve load select with filter by url_key and activity
     *
     * @param string $url_key
     * @param int $isActive
     * @return \Magento\Framework\DB\Select
     */
    protected function _getLoadByUrlKeySelect($url_key, $isActive = null)
    {
        $select = $this->getConnection()->select()->from(
            ['bp' => $this->getMainTable()]
        )->where(
            'bp.url_key = ?',
            $url_key
        );

        if (!is_null($isActive)) {
            $select->where('bp.is_active = ?', $isActive);
        }

        return $select;
    }

    /**
     *  Check whether post url key is numeric
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isNumericPostUrlKey(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[0-9]+$/', $object->getData('url_key'));
    }

    /**
     *  Check whether post url key is valid
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return bool
     */
    protected function isValidPostUrlKey(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/', $object->getData('url_key'));
    }

    /**
     * Check if post url key exists
     * return post id if post exists
     *
     * @param string $url_key
     * @return int
     */
    public function checkUrlKey($url_key)
    {
        $select = $this->_getLoadByUrlKeySelect($url_key, 1);
        $select->reset(\Zend_Db_Select::COLUMNS)->columns('bp.post_id')->limit(1);

        return $this->getConnection()->fetchOne($select);
    }

    /**
     * Select All realated categories associated to given storelocator
     *
     * @param \Kaliop\Blog\Model\Post $post
     * @return \Kaliop\Blog\Model\Post
     */
    public function getCategories($post)
    {
        $sql = 'SELECT * 
          FROM kaliop_blog_post_category 
          WHERE post_id = ' . $post->getId();

        $fetch = $this->getConnection()->fetchAll($sql);

        $categories = [];
        if ($fetch) {
            foreach ($fetch as $row) {
                $categories[] = $row['category_id'];
            }
        }

        $post->setData('categories', $categories);

        return $post;
    }

    /**
     * Get post category identifiers
     *
     * @param $post
     * @return array
     */
    public function getCategoryIds($post)
    {
        $sql = 'SELECT category_id 
          FROM kaliop_blog_post_category 
          WHERE post_id = ' . $post->getId();

        return $this->getConnection()->fetchCol($sql);
    }

    /** Look if user has read given post
     * @param array $data
     * @return array
     */
    public function isSeen($data)
    {
        $sql = 'SELECT * 
          FROM kaliop_blog_post_user 
          WHERE post_id = ' . $data['post_id'] .'
           AND entity_id = ' . $data['entity_id'];

        $fetch = $this->getConnection()->fetchAll($sql);

        return $fetch;

    }

    /** Receive list of read posts for user
     * @param int $userId
     * @return array
     */
    public function getReadIds($userId)
    {
        $sql = 'SELECT * 
          FROM kaliop_blog_post_user 
          WHERE entity_id = ' . $userId;

        $fetch = $this->getConnection()->fetchCol($sql);
//        var_dump($fetch);die;
        return $fetch;
    }

    public function setSeen($data)
    {
        if($this->isSeen($data) == null){
            $write = $this->getConnection();
            $write->insertMultiple('kaliop_blog_post_user', $data);
        };
    }

    public function unsetSeen($data)
    {
        $sql = 'DELETE FROM kaliop_blog_post_user
                WHERE entity_id = '. $data['entity_id'] .'
                AND post_id = '. $data['post_id'];
        return $this->getConnection()->query($sql);
    }

}
