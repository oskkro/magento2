<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 14:38
 */

namespace Kaliop\Blog\Model\ResourceModel\Readers;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Kaliop\Blog\Model\ResourceModel\Category
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'kaliop_blog_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'readers_collection';

    protected function _construct()
    {
        $this->_init('Kaliop\Blog\Model\Readers', 'Kaliop\Blog\Model\ResourceModel\Readers');
    }

//    protected function _initSelect()
//    {
//    parent::_initSelect();
//    $this->getSelect()->joinLeft(
//        ['user_table' => $this->getTable('customer_entity')],
//        'main_table.entity_id = user_table.entity_id',
//        '*'
//    );
//    $this->getSelect()->joinLeft(
//        ['post_table' => $this->getTable('kaliop_blog_post')],
//        'main_table.post_id = user_table.post_id',
//        '*'
//    );
//
//    }
}

