<?php

namespace Kaliop\Blog\Model\ResourceModel\Post;

use Kaliop\Blog\Model\Post;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Kaliop\Blog\Model\Post', 'Kaliop\Blog\Model\ResourceModel\Post');
    }

    /**
     * Add store availability filter. Include availability product
     * for store website
     *
     * @param null|string|bool|int|Post $post
     * @return $this
     */
    public function addPostFilter($post = null)
    {
        if (!$this->getFlag('store_filter_added')) {
            if ($post instanceof Post) {
                $post = [$post->getId()];
            }

            if (!is_array($post)) {
                $post = [$post];
            }

            $this->addFilter('post', ['in' => $post], 'public');
        }

        return $this;
    }

    /**
     * Add category filter
     *
     * @param int|array $categoryIds
     * @return $this
     */
    public function addCategoryFilter($categoryIds)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = [$categoryIds];
        }

        $this->getSelect()->join(
            ['category_table' => $this->getTable('kaliop_blog_post_category')],
            'main_table.post_id = category_table.post_id',
            []
        )->group('main_table.post_id');

        $this->getSelect()->where('category_table.category_id IN (?)', $categoryIds);

        return $this;
    }

    public function addReadFilter($userId)
    {
        $this->getSelect()->join(
            ['user_table' => $this->getTable('kaliop_blog_post_user')],
            'main_table.post_id = user_table.post_id',
            []
        )->group('main_table.post_id');

        $this->getSelect()->where('user_table.entity_id = ?', $userId);
        return $this;
    }

    public function addUnreadFilter($userId)
    {
        $sql = 'SELECT post_id
        FROM kaliop_blog_post_user
        WHERE entity_id = '.$userId;
        $fetch = $this->getConnection()->fetchCol($sql);

        if(!empty($fetch)){
            $this->getSelect()->where('main_table.post_id NOT IN (?)', $fetch);
        }
//        die($this->getSelectSql());
        return $this;
    }
}
