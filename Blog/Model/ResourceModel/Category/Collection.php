<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Model\ResourceModel\Category;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Kaliop\Blog\Model\ResourceModel\Category
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'category_id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'kaliop_blog_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'category_collection';

    protected function _construct()
    {
        $this->_init('Kaliop\Blog\Model\Category', 'Kaliop\Blog\Model\ResourceModel\Category');
    }
}
