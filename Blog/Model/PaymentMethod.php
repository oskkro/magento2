<?php

namespace Kaliop\Blog\Model;

use Magento\Payment\Model\Method\AbstractMethod;
//use Magento\Payment\Model\Method\Cc;

/**
 * Pay In Store payment method model
 */
class PaymentMethod extends AbstractMethod
{
    const METHOD_CODE = 'testpayment';

    protected $_code = self::METHOD_CODE;

}
