<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Model;

use Exception;
use Kaliop\Blog\Model\Post;
use Magento\Framework\Intl\DateTimeFactory;
use Kaliop\Blog\Model\Post as PostModel;
use Kaliop\Blog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchResultsInterfaceFactory;

/**
 * Class PostRepository
 * @package Kaliop\Blog\Model
 */
class PostRepository
{
    /**
     * @var PostFactory
     */
    private $postFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * PostRepository constructor.
     * @param PostFactory $postFactory
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param DateTimeFactory $dateTimeFactory
     */
    public function __construct(
        PostFactory $postFactory,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        DateTimeFactory $dateTimeFactory
    ) {
        $this->postFactory = $postFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dateTimeFactory = $dateTimeFactory;
    }

    /**
     * Process event passed with request and save into DB.
     *
     * @param array $requestParams
     * @return array
     */
    public function processAndSaveFromRequest($requestParams)
    {
        $postData = $requestParams['post']['general'];
        $postData = $this->prepareDates($postData);

        if (isset($postData['post_id']) && $postData['post_id'] > 0) {
            $post = $this->getById($postData['post_id']);
            $post->addData($postData);
        } else {
            unset($postData['post_id']);
            $post = $this->postFactory->create();
            $post->addData($postData);
        }

        $post->setData('categories',
            isset($requestParams['post']['categories']) ? $requestParams['post']['categories'] : null);

        $this->save($post);

        $result = [
            'error' => false,
            'message' => 'Post Saved.'
        ];

        if (!$post->getId()) {
            $result['error'] = true;
        } else {
            $result['id'] = $post->getId();
        }

        return $result;
    }

    /**
     * Mass status update for selected stores.
     *
     * @param int $newStatus
     * @param array $postsToUpdate
     * @param bool $includeAction
     */
    public function changePostStatus($newStatus, $postsToUpdate, $includeAction)
    {
        $whereCond = '';
        $writeAdapter = $this->postFactory->create()->getResource()->getConnection();
        if (!empty($postsToUpdate)) {

            if ($postsToUpdate != false && $postsToUpdate != 'false') {
                $whereIn = '(' . implode(',', $postsToUpdate) . ')';
                $action = $includeAction ? 'IN' : 'NOT IN';
                $whereCond = "post_id {$action} {$whereIn}";
            }

            $writeAdapter->update('kaliop_blog_post', ['is_active' => $newStatus],
                $whereCond);
        }
    }

    /**
     * Prepare created at and updated at fields
     *
     * @param array $postData
     * @return array
     */
    private function prepareDates(array $postData)
    {
        $now = $this->dateTimeFactory->create();
        if (empty($postData['created_at'])) {
            $postData['creation_time'] = $now;
        }
        $postData['update_time'] = $now;
        return $postData;
    }


    /**
     * @param PostModel $post
     * @return PostModel
     * @throws CouldNotSaveException
     */
    public function save(PostModel $post)
    {
        $now = $this->dateTimeFactory->create();
        $post->setCreationTime($now);
        $post->setUpdateTime($now);
//        $post->setUrlKey('so');
        $this->handleCategories($post);

        try {
            $post->getResource()->save($post);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $post;
    }
    /**
     * Handling saving categories ids
     *
     * @param Post $post
     */
    private function handleCategories(PostModel $post)
    {
        $savedCategories = $post->getData('categories');

        if (is_null($savedCategories)) {
            return;
        }

        if ($savedCategories) {
            $savedCategories = explode('&', $savedCategories);
            $onKey = array_search('on', $savedCategories);
            if ($onKey) {
                unset($savedCategories[$onKey]);
            }
        } else {
            $savedCategories = [];
        }

        $oldCategoryIds = $post->getCategoryIds();

        $insert = array_diff($savedCategories, $oldCategoryIds);
        $delete = array_diff($oldCategoryIds, $savedCategories);

        $write = $post->getResource()->getConnection();
        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $categoryId) {
                if (empty($categoryId)) {
                    continue;
                }
                $data[] = [
                    'category_id' => (int)$categoryId,
                    'post_id' => (int)$post->getId()
                ];
            }
            if ($data) {
                $write->insertMultiple('kaliop_blog_post_category', $data);
            }
        }

        if (!empty($delete)) {
            foreach ($delete as $categoryId) {
                $where = [
                    'post_id = ?' => (int)$post->getId(),
                    'category_id = ?' => (int)$categoryId,
                ];

                $write->delete('kaliop_blog_post_category', $where);
            }
        }
    }

    /**
     * Get Post by ID.
     *
     * @param integer $id
     * @return PostModel
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        /** @var PostModel $object */
        $object = $this->postFactory->create();
        $object->getResource()->load($object, $id);

        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }

        return $object;
    }

    /**
     * Delete Event with all related data.
     *
     * @param PostModel $object
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(PostModel $object)
    {
        try {
            $object->getResource()->delete($object);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param integer $id
     * @return bool
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
