<?php

namespace Kaliop\Blog\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Category
 * @package Kaliop\Blog\Model
 */
class Category extends AbstractModel implements IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = 'post_category';

    /**
     * @var string
     */
    const ENTITY_REGISTRY_NAME = 'current_post_category';


    protected function _construct()
    {
        $this->_init('Kaliop\Blog\Model\ResourceModel\Category');
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}

