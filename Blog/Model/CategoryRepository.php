<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Model;

use Exception;
use Kaliop\Blog\Model\Category as CategoryModel;
use Kaliop\Blog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchResultsInterfaceFactory;

/**
 * Class CategoryRepository
 * @package Kaliop\Blog\Model
 */
class CategoryRepository
{
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * CategoryRepository constructor.
     * @param CategoryFactory $categoryFactory
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        CategoryFactory $categoryFactory,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Process category passed with request and save into DB.
     *
     * @param array $requestParams
     * @return array
     */
    public function processAndSaveFromRequest($requestParams)
    {
//        var_dump($requestParams);die;
        if (isset($requestParams['category_id']) && $requestParams['category_id'] > 0) {
            $category = $this->getById($requestParams['category_id']);
            $category->addData($requestParams);
        } else {
            unset($requestParams['category_id']);
            $category = $this->categoryFactory->create();
            $category->addData($requestParams);
        }

        $this->save($category);

        $result = [
            'error' => false,
            'message' => 'Category Saved.'
        ];

        if (!$category->getId()) {
            $result['error'] = true;
        } else {
            $result['id'] = $category->getId();
        }

        return $result;
    }

    /**
     * Process data from inline editor.
     *
     * @param array $inlineEditItems
     * @return array
     */
    public function saveFromInline($inlineEditItems)
    {
        $result = [
            'error' => false,
            'message' => []
        ];

        foreach ($inlineEditItems as $id => $item) {
            if ($id == $item['category_id']) {
                $category = $this->getById($id);
                if ($category->getId() == $id) {
                    $category->setData('title', $item['title']);
                    $this->save($category);
                    $category->unsetData();
                } else {
                    $result['error'] = true;
                    $result['message'][] = __("Category ID {$id} was not processed. Try again.");
                }
            } else {
                $result['error'] = true;
                $result['message'][] = __("Category ID does not match: {$id} != {$item['category_id']}. Try again.");
                break;
            }
        }

        return $result;
    }

    /**
     * @param CategoryModel $category
     * @return CategoryModel
     * @throws CouldNotSaveException
     */
    public function save(CategoryModel $category)
    {
        try {
            $category->getResource()->save($category);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $category;
    }

    /**
     * Get Category by ID.
     *
     * @param integer $id
     * @return CategoryModel
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        /** @var CategoryModel $object */
        $object = $this->categoryFactory->create();
        $object->getResource()->load($object, $id);

        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }

        return $object;
    }

    /**
     * Delete Category with all related data.
     *
     * @param CategoryModel $object
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(CategoryModel $object)
    {
        try {
            $object->getResource()->delete($object);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param integer $id
     * @return bool
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
