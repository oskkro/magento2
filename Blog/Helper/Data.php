<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Kaliop\Blog\Model\Post;

/**
 * Class Data
 * @package Kaliop\Blog\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $configInterface;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
        $this->configInterface = $context->getScopeConfig();
        $this->urlBuilder = $context->getUrlBuilder();
    }

    /**
     * Get url key from configuration
     *
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->configInterface->getValue(
            Config::XML_PATH_KALIOP_BLOG_URL_KEY,
            ScopeInterface::SCOPE_STORE);
    }
}
