<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Helper;

/**
 * Class Config
 * @package Kaliop\Blog\Helper
 */
class Config
{
    /**
     * Kaliop Blog Config variables path
     */
    const XML_PATH_KALIOP_BLOG_API_KEY = 'kaliop_blog/general/api_key';
    const XML_PATH_KALIOP_BLOG_ENABLED = 'kaliop_blog/general/is_enabled';
    const XML_PATH_KALIOP_BLOG_URL_KEY = 'kaliop_blog/general/url_key';

}
