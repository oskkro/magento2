<?php namespace Kaliop\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('kaliop_blog_post'))
            ->addColumn(
                'post_id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true],
                'Post ID'
            )
            ->addColumn(
                'url_key',
                Table::TYPE_TEXT,
                100,
                [
                    'nullable' => true,
                    'default' => null]
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false],
                'Blog Title'
            )
            ->addColumn('content',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Blog Content')
            ->addColumn('is_active',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'default' => '1'],
                'Is Post Active?')
            ->addColumn(
                'creation_time',
                Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false],
                'Creation Time'
            )
            ->addColumn('update_time',
                Table::TYPE_DATETIME,
                null,
                [
                    'nullable' => false],
                'Update Time'
            )
            ->addIndex($installer->getIdxName('blog_post', ['url_key']), ['url_key'])
            ->setComment('Kaliop Blog Posts');

        $installer->getConnection()->createTable($table);

        $tableCategory = $setup->getConnection()->newTable(
            $setup->getTable('post_category'));

        $tableCategory->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'nullable' => false,
                'primary' => true,
                'unsigned' => true
            ],
            'Category ID'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            [
                'nullable' => false
            ],
            'Category title'
        );
        $installer->getConnection()->createTable($tableCategory);

        /**
         * Create table 'kaliop_blog_post_category'
         */
        if (!$setup->tableExists('kaliop_blog_post_category')) {
            $tableRelations = $setup->getConnection()->newTable(
                $setup->getTable('kaliop_blog_post_category'));
            $tableRelations->addColumn(
                'post_id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true
                ],
                'Post ID'
            )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true
                    ],
                    'Category ID'
                )->setComment('Kaliop Blog Category Linkage Table')
                ->addForeignKey(
                    $setup->getFkName(
                        'kaliop_blog_post_category',
                        'post_id',
                        'kaliop_blog_post',
                        'post_id'),
                    'post_id',
                    $setup->getTable('kaliop_blog_post'),
                    'post_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'kaliop_blog_post_category',
                        'category_id',
                        'post_category',
                        'category_id'),
                    'category_id',
                    $setup->getTable('post_category'),
                    'category_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Post to Category Many to Many');

            $setup->getConnection()->createTable($tableRelations);
        }
        /**
         * Create table 'kaliop_blog_post_user'
         */
        if (!$setup->tableExists('kaliop_blog_post_user')) {
            $tableRelationsUser = $setup->getConnection()->newTable(
                $setup->getTable('kaliop_blog_post_user'));
            $tableRelationsUser->addColumn(
                'post_id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'nullable' => false,
                    'primary' => true,
                    'unsigned' => true
                ],
                'Post ID'
            )
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true
                    ],
                    'Category ID'
                )->setComment('Kaliop Blog Category Linkage Table')
                ->addForeignKey(
                    $setup->getFkName(
                        'kaliop_blog_post_user',
                        'post_id',
                        'kaliop_blog_post',
                        'post_id'),
                    'post_id',
                    $setup->getTable('kaliop_blog_post'),
                    'post_id',
                    Table::ACTION_CASCADE
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'kaliop_blog_post_user',
                        'entity_id',
                        'customer_entity',
                        'entity_id'),
                    'entity_id',
                    $setup->getTable('customer_entity'),
                    'entity_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Post to Customer Many to Many');

            $setup->getConnection()->createTable($tableRelationsUser);
        }
        $installer->endSetup();
    }
}
