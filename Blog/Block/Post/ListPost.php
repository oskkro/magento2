<?php

namespace Kaliop\Blog\Block\Post;

use Kaliop\Blog\Model\ResourceModel\Post\Collection as PostCollection;


class ListPost extends AbstractPost
{
    /**
     * Checked categories
     *
     * @var null|bool|array
     */
    protected $_checkedCategories;

    protected function _construct()
    {
        parent::_construct();

        // Populate checked categories from request
        $checkedCategories = $this->getRequest()->getParam('categories');
        if (is_null($checkedCategories) || empty($checkedCategories)) {
            $this->_checkedCategories = false;
        } else {
            $this->_checkedCategories = explode('|', $checkedCategories);
        }
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Check if we must check a category checkbox
     *
     * @param int $categoryId
     * @return bool
     */
    public function isCategoryChecked($categoryId)
    {
        if ($this->_checkedCategories !== false) {
            return in_array($categoryId, $this->_checkedCategories);
        }

        return false;
    }

    /**
     * Add filter by categories
     *
     * @param PostCollection $collection
     * @return PostCollection
     */
    protected function _prepareCollection($collection)
    {
        $collection = parent::_prepareCollection($collection);
        $collection = $this->_filterCheckedCategories($collection);
        return $collection;
    }

    /**
     * Filter categories based on checked categories on list
     *
     * @param PostCollection $collection
     * @return PostCollection
     */
    protected function _filterCheckedCategories($collection)
    {
        if (!$this->getCategoryCollection()->count() || $this->_checkedCategories === false) {
            return $collection;
        }

        $collection->addCategoryFilter($this->_checkedCategories);

        return $collection;
    }

    /**
     * Get search query (from search form block)
     *
     * @return mixed
     */
    public function getSearchQuery()
    {
        return $this->getRequest()->getParam('query', false);
    }
}
