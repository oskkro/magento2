<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Post;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Data\Collection;
use Magento\Framework\Locale\ListsInterface;
use Magento\Framework\Locale\TranslatedLists;
use Magento\Framework\UrlInterface;
use \Magento\Framework\View\Element\Template;
use Kaliop\Blog\Helper\Data;
use Kaliop\Blog\Model\PostFactory;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\ResourceModel\Post\Collection as PostCollection;
use Kaliop\Blog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Zend_Db_Select;

class AbstractPost extends Template
{
    /**
     * @var
     */
    protected $_postCollection;

    /**
     * @var
     */
    protected $_categoryCollection;

    /**
     * @var PostFactory
     */
    protected $_postFactory;

    /**
     * @var  CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var ListsInterface
     */
    protected $_translatedListsInterface;

    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\Collection
     */
    protected $_countryCollection;

    /**
     * Nb of stores for default country
     *
     * @var int
     */
    protected $_nbOfStoresForDefaultCountry;

    /**
     * @var JsonHelper;
     */
    protected $_jsonHelper;

    /**
     * AbstractStore constructor.
     * @param Template\Context $context
     * @param array $data
     * @param PostFactory $postFactory
     * @param CategoryFactory $categoryFactory
     * @param TranslatedLists $translatedListsInterface
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        Template\Context $context,
        array $data = [],
        PostFactory $postFactory,
        CategoryFactory $categoryFactory,
        TranslatedLists $translatedListsInterface,
        JsonHelper $jsonHelper
    ) {
        parent::__construct($context, $data);
        $this->_postFactory = $postFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_translatedListsInterface = $translatedListsInterface;
        $this->_jsonHelper = $jsonHelper;
    }

    public function getCollection()
    {
        return $this->_getCollection();
    }

    /**
     * Create category collection
     *
     * @return CategoryCollection
     */
    public function getCategoryCollection()
    {
        if (is_null($this->_categoryCollection)) {
            $collection = $this->_categoryFactory->create()->getCollection();
            $this->_categoryCollection = $collection;
        }

        return $this->_categoryCollection;
    }

    /**
     * Create collection object
     *
     * @return PostCollection
     */
    protected function _getCollection()
    {
        if ($this->_postCollection === null) {
            /** @var PostCollection $collection */
            $collection = $this->_postFactory->create()->getCollection();
            $this->_prepareCollection($collection);
            $this->_postCollection = $collection;
        }

        return $this->_postCollection;
    }

    /**
     * Prepare collection
     *
     * @param PostCollection $collection
     * @return PostCollection
     */
    protected function _prepareCollection($collection)
    {
        $categoryId = $this->_request->getParam('categoryId');
        if($categoryId != null){
            $collection->addCategoryFilter($categoryId);
        }
        return $collection;
    }

    /**
     * Get a list of fields which need to be selected
     *
     * @return array
     */
    protected function _getFieldToSelect()
    {
        $fields = [
            ['id' => 'post_id'],
            'title',
            'content',
            'creation_time'
        ];

        return $fields;
    }

    /**
     * Order by fields
     *
     * @return array
     */
    protected function _getOrderFields()
    {
        $order = [
            'creation_time' => Collection::SORT_ORDER_ASC,
        ];

        return $order;
    }

    /**
     * Get collection in JSON format
     *
     * @return string
     */
    public function getJsonCollection()
    {
        return $this->_jsonHelper->jsonEncode($this->_getCollection());
    }

}
