<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Post;

use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Locale\TranslatedLists;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Kaliop\Blog\Helper\Data;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\Post;
use Kaliop\Blog\Model\PostFactory;

class ViewPost extends AbstractPost
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var PostFactory
     */
    protected $_postFactory;

    public function __construct(
        Template\Context $context,
        array $data = [],
        PostFactory $postFactory,
        CategoryFactory $categoryFactory,
        TranslatedLists $translatedListsInterface,
        Data $storeHelper,
        JsonHelper $jsonHelper,
        Registry $registry
    ) {
        parent::__construct($context, $data, $postFactory, $categoryFactory, $translatedListsInterface,
             $jsonHelper);
        $this->registry = $registry;
        $this->_postFactory = $postFactory;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->registry->registry(Post::ENTITY_REGISTRY_NAME);

    }

    public function SetSeen($data)
    {
        if($this->_postFactory->create()->isSeen($data) == null){
            $write = $this->_postFactory->create()->getResource()->getConnection();
            $this->_postFactory->create()->getResource();
            $write->insertMultiple('kaliop_blog_post_user', $data);
        };
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}
