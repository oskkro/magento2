<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;

/**
 * Class SaveAndContinueButton
 * @package Kaliop\Blog\Block\Adminhtml\Post\Edit
 */
class SaveAndContinueButton extends GenericButton
{
    /**
     * Permission resource used for button enable/disable
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::blog_category_edit';

    /**
     * @var string
     */
    protected $buttonClasses = 'save';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Save and Continue Edit';

    /**
     * @var int
     */
    protected $buttonSortOrder = 80;


    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        $data = [
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ]
        ];

        return $data;
    }
}
