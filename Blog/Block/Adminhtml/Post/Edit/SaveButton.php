<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Krzysztof Majkowski <km@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;

/**
 * Class SaveButton
 * @package Kaliop\Blog\Block\Adminhtml\Post\Edit
 */
class SaveButton extends GenericButton
{
    /**
     * Permission ressource used for button enable/disable
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::post_save';

    /**
     * @var string
     */
    protected $buttonClasses = 'save primary';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Save Post';

    /**
     * @var int
     */
    protected $buttonSortOrder = 90;

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        $data = [
            'data_attribute' => [
                'mage-init' => ['button' => ['post' => 'save']],
                'form-role' => 'save',
            ],
        ];
        return $data;
    }
}
