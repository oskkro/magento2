<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;

/**
 * Class BackButton
 * @package Kaliop\Blog\Block\Adminhtml\Post\Edit
 */
class ResetButton extends GenericButton
{
    /**
     * @var string
     */
    protected $entityRegistryName = 'current_category';

    /**
     * @var string
     */
    protected $buttonClasses = 'reset';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Reset';

    /**
     * @var int
     */
    protected $buttonSortOrder = 15;

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        $data = [
            'on_click' => sprintf(
                "location.href = '%s';",
                $this->getUrl('*/*/*',
                    [
                        'id' => $this->getEntityId()
                    ]
                )
            ),
        ];

        if (!$this->getEntityId()) {
            $data['disabled'] = 'disabled';
        }

        return $data;
    }
}
