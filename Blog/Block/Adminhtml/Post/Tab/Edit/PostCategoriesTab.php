<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Tab\Edit;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Registry;
use Magento\Backend\Block\Template\Context;
use Magento\Ui\Component\Layout\Tabs\TabWrapper;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

/**
 * Class KaliopCategoriesTab
 * @package Kaliop\BLog\Block\Adminhtml\Post\Tab\Edit
 */
class PostCategoriesTab extends TabWrapper implements TabInterface
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry = null;

    /**
     * @var bool
     */
    protected $isAjaxLoaded = true;

    /**
     * @var AuthorizationInterface
     */
    private $auth;

    /**
     * PostCategoriesTab constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->auth = $context->getAuthorization();
        parent::__construct($context, $data);
    }

    /**
     * If tab can be displayed
     *
     * @return bool
     */
    public function canShowTab()
    {
//        $isAllowed = $this->auth->isAllowed('Kaliop_Blog::post_category_list');
        return true;
    }

    /**
     * Return Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Categories');
    }

    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('*/*/categories', ['_current' => true]);
    }
}
