<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 19.06.17
 * Time: 13:41
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Tab\Edit;

use Magento\Backend\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Customer\Model\CustomerFactory;
use Magento\Backend\Block\Widget\Grid\Extended;

class PostUserGrid extends Extended
{
    /**
     * @var Registry
     */
    private $coreRegistry = null;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * PostUserGrid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param CustomerFactory $customerFactory
     * @param Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CustomerFactory $customerFactory,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->customerFactory = $customerFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setData('id', 'kaliop_blog_post_user_grid');
        $this->setDefaultSort('post_id');
        $this->setData('use_ajax', true);
    }
    /**
     * Prepare grid columns.
     *
     * @return Extended
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'category_id',
            [
                'header' => __('Post ID'),
                'index' => 'post_id',
                'header_css_class' => 'data-grid-onoff-cell',
                'filter' => 'Magento\Backend\Block\Widget\Grid\Column\Filter\Range',
                'data' => 'see what happens'
            ]
        );

        $this->addColumn(
            'customer_id',
            [
                'header' => __('Customer ID'),
                'index' => 'customer_id',
                'header_css_class' => 'data-grid-onoff-cell',
                'filter' => 'Magento\Backend\Block\Widget\Grid\Column\Filter\Range'
            ]
        );


        return parent::_prepareColumns();
    }


    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/usergrid', ['_current' => true]);
    }
}
