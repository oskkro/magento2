<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Post\Tab\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Kaliop\Blog\Model\CategoryFactory;
use Kaliop\Blog\Model\Post;

/**
 * Class PostCategoryGrid
 * @package Kaliop\Blog\Block\Adminhtml\Post\Tab\Edit
 */
class PostCategoryGrid extends Extended
{
    /**
     * @var Registry
     */
    private $coreRegistry = null;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * PostCategoryGrid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param CategoryFactory $categoryFactory
     * @param Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CategoryFactory $categoryFactory,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setData('id', 'kaliop_blog_post_category_grid');
        $this->setDefaultSort('category_id');
        $this->setData('use_ajax', true);
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_categories') {
            $categoryIds = $this->getSelectedCategories();
            if (empty($categoryIds)) {
                $categoryIds = 0;
            }

            if ($column->getFilter()->getData('value')) {
                $this->getCollection()->addFieldToFilter('category_id', ['in' => $categoryIds]);
            } else {
                if ($categoryIds) {
                    $this->getCollection()->addFieldToFilter('category_id', ['nin' => $categoryIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->categoryFactory
            ->create()
            ->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns.
     *
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_categories',
            [
                'type' => 'checkbox',
                'html_name' => 'post[categories]',
                'required' => true,
                'values' => $this->getSelectedCategories(),
                'align' => 'center',
                'index' => 'category_id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'category_id',
            [
                'header' => __('Category ID'),
                'index' => 'category_id',
                'header_css_class' => 'data-grid-onoff-cell',
                'filter' => 'Magento\Backend\Block\Widget\Grid\Column\Filter\Range'
            ]
        );

        $this->addColumn(
            'title',
            [
                'header' => __('Name'),
                'index' => 'title',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );

        return parent::_prepareColumns();
    }


    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/categorygrid', ['_current' => true]);
    }

    /**
     * Get row url
     * @param  object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * Get categories that should be selected on the grid.
     *
     * @return array
     */
    private function getSelectedCategories()
    {
        $categories = $this->getData('categories');
        if (!is_array($categories)) {
            $categories = $this->getSelectedPostCategories();
        }
        return $categories;
    }

    /**
     * Retrieve selected store categories
     *
     * @return array
     */
    public function getSelectedPostCategories()
    {
        $categories = [];
        if ($this->coreRegistry->registry(Post::ENTITY_REGISTRY_NAME)) {
            /** @var Post $post */
            $post = $this->coreRegistry->registry(Post::ENTITY_REGISTRY_NAME);
            $post->getCategories();

            /** @var \Kaliop\Blog\Model\Post $post */
            $categories = $post->getData('categories');
        }

        return $categories;
    }
}
