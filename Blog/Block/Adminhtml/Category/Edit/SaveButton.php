<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Category\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;

/**
 * Class SaveButton
 * @package Kaliop\Blog\Block\Adminhtml\Category\Edit
 */
class SaveButton extends GenericButton
{
    /**
     * Permission resource used for button enable/disable
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::blog_category_save';

    /**
     * @var string
     */
    protected $buttonClasses = 'save primary';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Save Category';

    /**
     * @var int
     */
    protected $buttonSortOrder = 90;

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        $data = [
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
        ];
        return $data;
    }
}
