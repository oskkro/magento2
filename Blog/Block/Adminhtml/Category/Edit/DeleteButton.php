<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Category\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;
use Kaliop\Blog\Model\Category;

/**
 * Class DeleteButton
 * @package Kaliop\Blog\Block\Adminhtml\Category\Edit
 */
class DeleteButton extends GenericButton
{

    /**
     * Permission resource used for button enable/disable
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::blog_category_delete';

    /**
     * @var string
     */
    protected $entityRegistryName = Category::ENTITY_REGISTRY_NAME;

    /**
     * @var string
     */
    protected $buttonClasses = 'delete';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Delete Category';

    /**
     * @var int
     */
    protected $buttonSortOrder = 20;

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        $data = [
            'id' => 'category-edit-delete-button',
            'data_attribute' => [
                'url' => $this->getDeleteUrl()
            ],
            'on_click' => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
        ];
        if (!$this->getEntityId()) {
            $data['disabled'] = 'disabled';
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        if (!$this->getEntityId()) {
            return $this->getUrl('*/*/index');
        }
        return $this->getUrl('*/*/delete', ['id' => $this->getEntityId()]);
    }
}
