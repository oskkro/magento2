<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Category\Edit;

use Kaliop\Blog\Block\Adminhtml\Generic\Edit\GenericButton;

/**
 * Class BackButton
 * @package Kaliop\Blog\Block\Adminhtml\Category\Edit
 */
class BackButton extends GenericButton
{
    /**
     * @var string
     */
    protected $buttonClasses = 'back';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Back';

    /**
     * @var int
     */
    protected $buttonSortOrder = 10;

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        return [
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
        ];
    }
}
