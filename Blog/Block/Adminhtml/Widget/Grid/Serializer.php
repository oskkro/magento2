<?php
/**
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author Hervé Guétin <herve.guetin@gmail.com> <@herveguetin>
 * @copyright Copyright (c) 2016 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Widget\Grid;

use Magento\Backend\Block\Widget\Grid\Serializer as MagentoSerializer;

class Serializer extends MagentoSerializer
{
    /**
     * Set serializer template
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('Kaliop_Blog::widget/grid/serializer.phtml');
    }
}
