<?php
/**
 * This file is part of Soon_StoreLocator for Magento2.
 *
 * @license All rights reserved
 * @author Arkadiusz Tokarczyk <at@agence-soon.fr>
 * @category Soon
 * @package Soon_StoreLocator
 * @copyright Copyright (c) 2015 Agence Soon (http://www.agence-soon.fr)
 */

namespace Kaliop\Blog\Block\Adminhtml\Generic\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class GenericButton
 * @package Soon\StoreLocator\Block\Adminhtml\Generic\Edit
 */
abstract class GenericButton implements ButtonProviderInterface
{
    /**
     * Permission ressource used for button enable/disable
     */
    const PERMISSION_RESSOURCE_NAME = 'Kaliop_Blog::kaliop_blog';

    /**
     * @var string
     */
    protected $entityRegistryName = 'current_entity';

    /**
     * @var string
     */
    protected $buttonLabelSource = 'Button';

    /**
     * @var int
     */
    protected $buttonSortOrder = 0;

    /**
     * @var string
     */
    protected $buttonClasses = 'button';

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Registry
     *
     * @var Registry
     */
    protected $registry;

    /**
     * @var AuthorizationInterface
     */
    protected $auth;

    /**
     * GenericButton constructor.
     * @param Context $context
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
        $this->auth = $context->getAuthorization();
    }

    /**
     * Return the entity ID.
     *
     * @return int|null
     */
    public function getEntityId()
    {
        return ($this->registry->registry($this->entityRegistryName)
            ? $this->registry->registry($this->entityRegistryName)->getId() : false);
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        if (!$this->_isAllowed()) {
            return [];
        }

        $baseConfig = [
            'label' => __($this->buttonLabelSource),
            'class' => $this->buttonClasses,
            'sort_order' => $this->buttonSortOrder
        ];

        $additionalConfig = $this->getAdditionalConfig();


        return array_merge($baseConfig, $additionalConfig);
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }

    /**
     * Use this method to pass additional button settings.
     *
     * @return array
     */
    protected function getAdditionalConfig()
    {
        return [];
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->auth->isAllowed(self::PERMISSION_RESSOURCE_NAME);
    }
}
