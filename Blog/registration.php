<?php

/**
 * @author Arkadiusz Tokarczyk
 * @package Soon_StoreLocator
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kaliop_Blog',
    __DIR__
);
